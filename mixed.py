from dolfin import *
import numpy as np
set_log_level(30)


def solve_mixed(problem, viz=False, save_result=False):
	
	if save_result:
		ufile = File('results/u.pvd')
		pfile = File('results/p.pvd')
		
	mesh = problem.mesh
	
	V_order = problem.V_order
	Q_order = problem.Q_order
	
	V = VectorFunctionSpace(mesh, 'CG', V_order)
	Q = FunctionSpace(mesh, 'CG',Q_order)
	VQ = V * Q

	# Test and Trial functions
	u, p = TrialFunctions(VQ)
	v, q = TestFunctions(VQ)
	
	# Functions
	up0 = Function(VQ)
	u0, p0 = split(up0)
	up_1 = Function(VQ)
	u_1, p_1 = split(up_1)
	
	# For plotting
	u1 = Function(V)
	p1 = Function(Q)

	# Parameters
	t = 0.0
	bcu, bcp,bcc = problem.bcs(V, Q, t, VQ)
	bcs = []
	bcs.extend(bcu)
	bcs.extend(bcp)
	dt = problem.dt
	T = problem.T
	mu = problem.mu
	nu = problem.nu
	rho = problem.rho
	k = Constant(dt)
	
	u_cn = 0.5*(u + u0)
	u_ab = 1.5*u0 - 0.5*u_1
	
	# Variational problem
	F = 1./k*inner((u-u0),v)*dx + inner(dot(u_ab,nabla_grad(u_cn)),v)*dx + \
		nu*inner(grad(u_cn), grad(v))*dx - inner(div(v),p)*dx - \
		inner(q,div(u))*dx

	a = lhs(F)
	L = rhs(F)

	timestep = 0

	while t < T:
		print 't=',t
		problem.bc_update(t)
		
		# Solve
		A = assemble(a)
		b = assemble(L)
		[bc.apply(A, b) for bc in bcs]
		solve(A, up0.vector(),b)
		
		if problem.viz:
			# plotting and saving
			p1.assign(up0.split()[1])
			u1.assign(up0.split()[0])
			plot(u1)
			plot(p1)
		
		if problem.save_result:
			ufile << up0.split()[0]
			
		timestep += 1
		t += dt
		
		up_1.assign(up0)
		
		
	u, p = up0.split()
	return t, u, p