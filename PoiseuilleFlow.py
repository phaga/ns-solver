from ipcs import *
from ipcs_naive import *
from mixed import *
from dolfin import  *
set_log_level(30)

		
class PoiseuilleFlow():
	
	def __init__(self, mesh):
		self.rho = 1.
		self.mu = 1.
		self.nu = self.mu/self.rho
		self.mesh = mesh
		self.save_freq = 1
		self.dt = 0.01
		self.T = 0.5
		self.V_order = 2
		self.Q_order = 1
		print 'mesh size =', self.mesh.num_cells()
		
	def ics(self,V, Q):
		u0 = Expression(('0.0','0.0'))
		p0 = Expression('0.0')
		return [u0, p0]
	
	def bcs(self,V, Q, t, VQ=None):
		
		if VQ is not None:
			Vu = VQ.sub(0)
			Vp = VQ.sub(1)
		else:
			Vu = V
			Vp = Q
			
		def left(x, on_boundary):
			return near(x[0], 0.0)
		
		def right(x, on_boundary):
			return near(x[0], 1.0)
		
		def bottom(x, on_boundary):
			return near(x[1], 0.0)
		
		def top(x, on_boundary):
			return near(x[1], 1.0)
					

		bc_noslip_top = DirichletBC(Vu, Constant((0.0,0.0)), top)
		bc_noslip_bottom = DirichletBC(Vu, Constant((0.0,0.0)), bottom)
		bcp_inlet = DirichletBC(Vp, Constant(2.0), left)
		bcp_outlet = DirichletBC(Vp, Constant(0.0), right)
		
		
		bcu = [bc_noslip_bottom, bc_noslip_top]
		bcp = [bcp_inlet, bcp_outlet]
		
		return bcu, bcp
	
	def bc_update(self,t):
		pass

if __name__ == '__main__':
	import time
	solver = solve_mixed
	n = 20
	mesh = UnitSquareMesh(n,n)
	problem = PoiseuilleFlow(mesh)
	raw_input('dt=%g. Press [enter]... ' % problem.dt)
	
	start = time.time()
	t, u, p = solver(problem, viz=True, save_result=False)
	end = time.time()
	print 'time:', end - start
	
	pfile = File('p-final.pvd')
	pfile << p
	
	
	