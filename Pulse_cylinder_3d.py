from ipcs import *
from ipcs_naive import *
from mixed import *
from dolfin import  *
set_log_level(30)

class inlet_value(Expression):
	def __init__(self):
		self.t = 0.0
		self.f = 1.0   # Number of cycles per sec.
		self.omega = 2.0*pi*self.f
				
	def value_shape(self):
		return (3,)
			
	def eval(self, value, x):
		x0 = 0.0
		y0 = 0.0
		s_x = 0.3
		s_y = 0.3
		A = 3.0
		
		if self.t < 8.0:
			value[2] = A*sin(2*pi*self.f*self.t)*exp(-((x[0]-x0)**2/(2*s_x**2) + (x[1]-y0)**2/(2*s_y**2)))
		else:
			value[2] = 0.0
		value[0] = 0.0
		value[1] = 0.0


class c_init(Expression):
	def __init__(self):
		self.radius = 0.4 - 1e-12
	
	def eval(self, value, x):
		if x[0]**2 + (x[1]+1.0)**2 + (x[2]-5.0)**2 < self.radius**2:
			value[0] = 1.0
		else:
			value[0] = 0.0
		
		
class CylinderPulse():
	
	def __init__(self):
		self.mesh = Mesh('mesh/cylinder_3d.xml')
		#plot(self.mesh, interactive=True)
		self.rho = 1.0
		self.mu = 1./1000
		self.nu = self.mu/self.rho
		self.Schmidt = 1.0
		self.frequency = 1.0
		self.beta = 0.0 # turn velocity convection on/off
		self.l = 10.0
		self.r = 1.0
		self.radius = 0.4
		
		self.dt = 0.01
		print "Courant number: ",  self.dt * 1.0 / self.mesh.hmin()
		self.scalar = True
		
		self.T = 4.0
		self.V_order = 2
		self.Q_order = 1
		self.inlet = inlet_value()
		self.use_supg = True
		
		self.viz = False
		self.save_result = True
		self.save_freq = 1
		
		if self.save_result:
			self.ufile = File('results/u.pvd')
			self.pfile = File('results/p.pvd')
			if self.scalar: 
				self.cfile = File('results/c.pvd')
		
		print 'mesh size =', self.mesh.num_cells()
		
	def ics(self,V, Q):
		c0 = c_init()
		u0 = Expression(('0.0','0.0'))
		p0 = Expression('0.0')
		
		return [u0, p0, c0]
	
	def bcs(self,V, Q, t, VQ=None):
		
		if VQ is not None:
			Vu = VQ.sub(0)
			Vp = VQ.sub(1)
			Vc = VQ.sub(1)
		else:
			Vu = V
			Vp = Q
			Vc = Q
			
		def cyl_wall(x,on_boundary):
			return on_boundary and 1e-12 < x[2] < 10.0 - 1e-12
		
		def inlet_boundary(x,on_boundary):
			return near(x[2],0.0)
		
		def outlet_boundary(x,on_boundary):
			return near(x[2],self.l)
		
		
		bcu_inlet = DirichletBC(Vu, self.inlet, inlet_boundary)
		bcu_outlet = DirichletBC(Vu, self.inlet, outlet_boundary)
		bcu_noslip = DirichletBC(Vu, Constant(((0.0,0.0,0.0))),cyl_wall)
		bcp_outlet = DirichletBC(Vp, Constant(0.0), outlet_boundary)
		bcc_inlet = DirichletBC(Vc, Constant(0.0), inlet_boundary)
	

		bcu = [bcu_noslip,bcu_inlet]
		bcp = [bcp_outlet]
		bc_scalar = [bcc_inlet]
		
		return bcu, bcp, bc_scalar
	
	def bc_update(self,t):
		self.inlet.t = t
		
	def peri_solve(self,timestep, u_, p_, c_):
		''' Plot and save'''
		if self.viz:
			plot(u_)
			#plot(p_)
			if self.scalar: plot(c_)

		
		if self.save_result:
			if timestep % self.save_freq == 0:
				#self.ufile << u_
				#self.pfile << p_
				if self.scalar:
					self.cfile << c_
	def post_solve(self):
		pass

if __name__ == '__main__':
	import time
	
	solver = solve_ipcs_naive
	problem = CylinderPulse()
	raw_input('dt=%g. Press [enter]... ' % problem.dt)
	
	start = time.time()
	t, u, p = solver(problem)
	end = time.time()
	print 'time:', end - start
	
	pfile = File('p-final.pvd')
	pfile << p
	
	
	