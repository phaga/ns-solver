from dolfin import *

def setup(u, v, p, q, u_ab):
	'''Setting up matrices'''
	
	# Convection term
	a_conv = inner(v, dot(u_ab, nabla_grad(u)))*dx
	
	# Matrix for pressure update
	P = assemble(inner(grad(p),v)*dx)
	
	# Divergence matrix
	R = assemble(inner(div(u),q)*dx)
	
	# Mass matrix
	M = assemble(inner(u, v)*dx)
	
	# Stiffness matrix
	K = assemble(inner(grad(u), grad(v))*dx)
	
	# Pressure laplacian
	Ap = assemble(inner(grad(q), grad(p))*dx) 
	
	return a_conv, P, R, M, K, Ap
	

def solve_ipcs(problem, viz=False, save_result=False):
	
	if save_result:
		ufile = File('results/u.pvd')
		pfile = File('results/p.pvd')
		
	mesh = problem.mesh
	
	V_order = problem.V_order
	Q_order = problem.Q_order

	# Define function spaces
	V = VectorFunctionSpace(mesh, "CG", V_order)
	Q = FunctionSpace(mesh, "CG", Q_order)

	# Get intial and boundary conditions
	t = 0.0
	#u0, p0 = problem.ics(V, Q)
	bcu, bcp, bcc = problem.bcs(V, Q, t)
	dt = problem.dt
	T = problem.T

	# Define test and trial functions
	v = TestFunction(V)
	q = TestFunction(Q)
	u = TrialFunction(V)
	p = TrialFunction(Q)
	
	# Functions
	u_ = Function(V) # Velocity at t
	u_1 = Function(V) # velocity at t - dt
	u_2 = Function(V) # velocity at t - 2*dt
	
	p_ = Function(Q) # Pressure at t
	p_1 = Function(Q) # Pressure at t - dt

	
	nu = problem.nu
	
	timestep = 1
	t = dt
	
	u_ab = 1.5*u_1 - 0.5*u_2
	a_conv, P, R, M, K, Ap = setup(u, v, p, q, u_ab)
	
	# Alocating matrices and vectors
	A = Matrix(M)
	A_rhs = Matrix(M)
	S = Matrix(M)
	S_rhs = Matrix(M)
	Au = Matrix(M)
	b_tmp = Vector(u_.vector())
	b = Vector(u_.vector())
	bu = Vector(u_.vector())
	bp = Vector(p_.vector())
	dp = Vector(p_.vector())
	
	while t <= T:
		print t
		problem.bc_update(t)
		
		# assemble convective term
		Ac = assemble(a_conv) 
		
		# Tentative velocity, lhs
		A.zero()
		A.axpy(1./dt,M,True)
		A.axpy(0.5,Ac,True)
		A.axpy(0.5*nu,K,True)
		
		# tentative velocity, rhs
		A_rhs.zero()
		A_rhs.axpy(1./dt,M,True)
		A_rhs.axpy(-0.5,Ac,True)
		A_rhs.axpy(-0.5*nu,K,True)
		b  = A_rhs*u_1.vector()
		b.axpy(-1.0, P * p_1.vector())
		
		[bc.apply(A, b) for bc in bcu]
		solve(A, u_.vector(), b, 'gmres', 'jacobi')
		
		# Pressure correction
		bp.zero()
		bp.axpy(1.0,Ap*p_1.vector())
		bp.axpy(-1./dt, R*u_.vector())
		[bc.apply(Ap, bp) for bc in bcp]
		solve(Ap, p_.vector(), bp, 'gmres', 'hypre_amg')
		
		# Velocity correction
		dp.zero()
		dp.axpy(1.0, p_.vector())
		dp.axpy(-1.0,p_1.vector())
		Au = M
		bu.zero()
		bu.axpy(1.0,M*u_.vector())
		bu.axpy(-dt,P*dp)
		[bc.apply(Au, bu) for bc in bcu]
		solve(Au,u_.vector(),bu, 'gmres', 'ilu')

		
		# Update
		u_2.vector().zero()
		u_2.vector().axpy(1.0, u_1.vector())
		
		u_1.vector().zero()
		u_1.vector().axpy(1.0, u_.vector())
		
		p_1.vector().zero()
		p_1.vector().axpy(1.0, p_.vector())
		
		problem.peri_solve(timestep, u_, p_, None)

		timestep += 1
		t = timestep*dt
	if viz: 
		interactive()
	return t, u_, p_	
	

if __name__ == '__main__':
	pass