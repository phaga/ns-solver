from dolfin import *


def solve_ipcs_naive(problem):
	
	mesh = problem.mesh
	scalar = problem.scalar
	if scalar: Schmidt = problem.Schmidt
	V_order = problem.V_order
	Q_order = problem.Q_order

	
	# Define function spaces
	V = VectorFunctionSpace(mesh, "CG", V_order)
	Q = FunctionSpace(mesh, "CG", Q_order)

	# Define test and trial functions
	v = TestFunction(V)
	q = TestFunction(Q)
	u = TrialFunction(V)
	p = TrialFunction(Q)
	c = TrialFunction(Q)
	c_t = TestFunction(Q)
	
	# Functions
	u_ = Function(V) # Velocity at t
	u_1 = Function(V) # velocity at t - dt
	u_2 = Function(V) # velocity at t - 2*dt
	
	p_ = Function(Q) # Pressure at t
	p_1 = Function(Q) # Pressure at t - dt
	
	c_ = Function(Q)
	c_1 = Function(Q)
	
	# Get intial and boundary conditions
	t = 0.0
	u0, p0, c0 = problem.ics(V, Q)
	c_1.assign(c0)
	bcu, bcp, bcc = problem.bcs(V, Q, t)
	dt = problem.dt
	T = problem.T
	
	# Parameters
	nu = Constant(problem.nu)
	k = Constant(dt)
	
	u_ab = Constant(1.5)*u_1 - Constant(0.5)*u_2
	u_cn = Constant(0.5)*(u + u_1)
	
	
	# Tentative velocity step
	F1 = (1./k)*inner(u-u_1,v)*dx + problem.beta*inner(dot(u_ab, nabla_grad(u_cn)), v)*dx + \
		nu*inner(grad(u_cn), grad(v))*dx + inner(grad(p_1),v)*dx

	a1 = lhs(F1)
	L1 = rhs(F1)
	
	# Pressure correction
	a2 = inner(grad(p),grad(q))*dx
	L2 = inner(grad(p_1),grad(q))*dx - (1./k)*div(u_)*q*dx

	# Velocity correction
	a3 = inner(u,v)*dx
	L3 = inner(u_,v)*dx - k*inner(grad(p_-p_1),v)*dx
	
	# Scalar with SUPG
	if scalar:
		c_cn = 0.5*(c + c_1)
		h = CellSize(mesh)
		if problem.use_supg:
			cw = c_t + h*inner(grad(c_t),u_ab)
		else:
			cw = c_t
			
		F4 = (1./dt)*inner(c - c_1, cw)*dx + inner(dot(nabla_grad(c_cn), u_ab), cw)*dx \
			+nu/Schmidt*inner(grad(c_cn), grad(cw))*dx
		a4 = lhs(F4)
		L4 = rhs(F4)
	
	# Assemble matrices
	A1 = assemble(a1)
	A2 = assemble(a2)
	A3 = assemble(a3)
	if scalar:
		A4 = assemble(a4)
	
	timestep = 1
	t = dt
	
	while t <= T:
		print t
		problem.bc_update(t)
		
		# Compute tentative velocity
		begin("Tentative velocity")
		A1, b = assemble_system(a1, L1)
		[bc.apply(A1, b) for bc in bcu]
		solve(A1, u_.vector(), b, 'gmres', 'jacobi')
		end()
		
		# Compute p1
		begin("Pressure corr")
		b = assemble(L2)
		if len(bcp) == 0: normalize(b)
		[bc.apply(A2, b) for bc in bcp]
		solve(A2, p_.vector(), b, 'gmres', 'hypre_amg')
		if len(bcp) == 0: normalize(p_.vector())
		end()
		
		# Compute u1
		begin("True velocity")
		b = assemble(L3)
		[bc.apply(A3, b) for bc in bcu]
		solve(A3, u_.vector(), b,'gmres', 'ilu')
		end()
		
		# compute scalar
		if scalar:
			begin("Scalar")
			solve(a4 == L4, c_, bcc)
			end()
		
		# Update
		u_2.assign(u_1)
		u_1.assign(u_)
		p_1.assign(p_)
		
		if scalar:
			c_1.assign(c_)
		
		problem.peri_solve(timestep, u_, p_, c_)

		timestep += 1
		t = timestep*dt
	
	problem.post_solve()
	
	return t, u_, p_	
	

if __name__ == '__main__':
	pass