from ipcs import *
from ipcs_naive import *
from mixed import *
from dolfin import  *
set_log_level(30)

class inlet_value(Expression):
	def __init__(self):
		self.t = 0.0
				
	def value_shape(self):
		return (2,)
			
	def eval(self, value, x):
		U_m = 1.5
		H = 0.41
		if self.t < 8.0:
			value[0] = 200*U_m*x[1]*(H - x[1])/H**2
		else:
			value[0] = 0.0
		value[1] = 0.0

class scalar_value(Expression):
	def __init__(self):
		self.t = 0.0
				
	#def value_shape(self):
	#	return (1,)
			
	def eval(self, value, x):
		U_m = 1.5
		H = 0.41
		value[0] = 4*U_m*x[1]*(H - x[1])*abs(sin(pi*self.t))/H**2


		
		
class CylinderFlow():
	
	def __init__(self, mesh):
		self.mesh = mesh
		
		self.rho = 1.
		self.mu = 1./1000
		self.nu = self.mu/self.rho
		self.Schmidt = 1000.0
		
		self.dt = 0.0001
		self.scalar = False
		
		self.T = 8.0
		self.V_order = 1
		self.Q_order = 1
		self.inlet = inlet_value()
		self.scalar_v = scalar_value()
		
		self.viz = True
		self.save_result = False
		self.save_freq = 5
		
		if self.save_result:
			self.ufile = File('results/u.pvd')
			self.pfile = File('results/p.pvd')
			if self.scalar: 
				self.cfile = File('results/c.pvd')
		
		print 'mesh size =', self.mesh.num_cells()
		
	def ics(self,V, Q):
		u0 = Expression(('0.0','0.0'))
		p0 = Expression('0.0')
		return [u0, p0]
	
	def bcs(self,V, Q, t, VQ=None):
		
		if VQ is not None:
			Vu = VQ.sub(0)
			Vp = VQ.sub(1)
			Vc = VQ.sub(1)
		else:
			Vu = V
			Vp = Q
			Vc = Q
			
		def left(x, on_boundary):
			return near(x[0], 0)
		
		def right(x, on_boundary):
			return near(x[0], 2.2)
		
		def bottom(x, on_boundary):
			return near(x[1], 0)
		
		def top(x, on_boundary):
			return near(x[1], 0.41)
		
		def scalar_inlet(x, on_boundary):
			return near(x[0], 0.0) and (0.16 < x[1] < 0.18 or 0.22 < x[1] < 0.24)
					
		
		def cylinder(x, on_boundary):
			return DOLFIN_EPS < x[0] < 2.2 - DOLFIN_EPS \
				and 0. + DOLFIN_EPS < x[1] < 0.41 - DOLFIN_EPS\
				and on_boundary
		
		
		bc_inlet = DirichletBC(Vu, self.inlet, left)
		bc_noslip_top = DirichletBC(Vu, Constant((0.0,0.0)), top)
		bc_noslip_bottom = DirichletBC(Vu, Constant((0.0,0.0)), bottom)
		bc_circle = DirichletBC(Vu, Constant((0.0,0.0)), cylinder)
		
		bcc0 = DirichletBC(Vc, Constant(0.0), left)
		bcc1 = DirichletBC(Vc, self.scalar_v, scalar_inlet)
		
		bcp_outlet = DirichletBC(Vp, Constant(0.0), right)

		bcu = [bc_noslip_top, bc_noslip_bottom, bc_circle, bc_inlet]
		bcp = [bcp_outlet]
		bc_scalar = [bcc0, bcc1]
		
		return bcu, bcp, bc_scalar
	
	def bc_update(self,t):
		self.inlet.t = t
		self.scalar_v.t = t
		
	def peri_solve(self,timestep, u_, p_, c_):
		''' Plot and save'''
		if self.viz:
			plot(u_)
			#plot(p_)
			if self.scalar: plot(c_)
		
		if self.save_result:
			if timestep % self.save_freq == 0:
				self.ufile << u_
				#self.pfile << p_
				if self.scalar:
					self.cfile << c_
	def post_solve(self):
		pass

if __name__ == '__main__':
	import time
	
	solver = solve_ipcs
	mesh = Mesh('mesh/cyl_fine.xml')
	problem = CylinderFlow(mesh)
	raw_input('dt=%g. Press [enter]... ' % problem.dt)
	
	start = time.time()
	t, u, p = solver(problem)
	end = time.time()
	print 'time:', end - start
	
	pfile = File('p-final.pvd')
	pfile << p
	
	
	